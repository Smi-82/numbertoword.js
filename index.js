var numberToWord = (function () {
     var lang = "ru";
     var result;
     var alg, number;
     var helpers = require("./helpers");

     function _init(l) {
          if (l != undefined)
               lang = l;
          alg = require("./lib/" + lang + "_smi");
     }

     function _getString() {
          return result.join(" ");
     }

     function _getArray() {
          return result;
     }

     function _validate() {
          number += "";
          number = number.trim();
          if (number == "undefined" || number == "")
               return helpers.errors.missNumb[lang];
          else if (/\D/.test(number))
               return helpers.errors.nonNumeric[lang];
          return true;
     }

     return {
          init: function (lang) {
               _init(lang);
               return this;
          },
          convert: function (numb) {
               number = numb;
               var valid = _validate();
               if (valid != true)
                    throw new Error(helpers.errors.general[lang] + ": " + valid + "!");
               else
                    result = alg.convert(number).filter(function (x) {
                         return x !== undefined && x !== null;
                    });
               return this;
          },
          getString: function () {
               return _getString();
          },
          getArray: function () {
               return _getArray();
          },
          setGender: function (gand) {
               if (alg.hasOwnProperty('setGender'))
                    alg.setGender(gand);
               return this;
          }
     };
})();

if (module.parent)
     module.exports = numberToWord;
else
     console.log(numberToWord.init().setGender().convert(1234567890).getString());
