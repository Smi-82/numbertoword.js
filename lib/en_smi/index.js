module.exports = new (function () {
     var group = {
          ed: 1,
          dec: 2,
          sot: 3
     };
     var phr = require("./en");

     /**
      *
      * @param ind
      * @param start
      * @returns {boolean}
      */
     function isDigit(ind, start) {
          return !(ind < start || (ind - start) % 3 != 0);
     }

     /**
      *
      * @param key
      * @param ind
      * @returns {*}
      */
     function getText(key, ind) {
          return phr.text[key][ind];
     }

     /**
      *
      * @param ind
      * @returns {*}
      */
     function getExponent(ind) {
          ind = Math.round(ind > (phr.exponent.length - 1) * 3 + 1 ? 0 : ind / 3);
          return phr.exponent[ind];
     }

     /**
      *
      * @param numb
      * @returns {*}
      */
     this.convert = function (numb) {
          var result = [];
          var arr = numb.split("").reverse();
          var fl = false;
          var len = arr.length;
          if (len == 1 && arr[0] == 0) {
               result.push(getText("units", 0, false));
               return this;
          }
          var raz = 3;
          while (!isDigit(len, raz--));
          raz++;
          do {
               if (arr[len - 1] != 0 && len != 0) {
                    var key;
                    fl = true;
                    var index = arr[len - 1] - 1;
                    switch (raz) {
                         case group.dec:
                              if (arr[len - 1] < 2 && arr[len - 2] != 0) {
                                   key = "to20";
                                   index = arr[len - 2] - 1;
                                   arr[len - 2] = 0;
                              } else
                                   key = "decade";
                              break;
                         default :
                              key = "units";
                              index++;
                    }
                    result.push(getText(key, index));
                    if(key == "decade" && arr[len - 2] != 0){
                         result.push("-");
                    }
               }
               if (fl && isDigit(len, 3)) {
                    result.push('hundred');
                    if(arr[len-2] != 0 || arr[len-3] != 0){
                         result.push('and');
                    }
               }
               if (fl && isDigit(len, 4)) {
                    fl = false;
                    result.push(getExponent(len - 1));
               }
               raz == 1 ? raz = 3 : --raz;
          } while (--len);
          var newRes = [];
          for(var i = 0; i < result.length; i++){
               if(result[i] == "-"){
                    newRes.pop();
                    newRes.push(result[i-1] + result[i] + result[i+1]);
                    delete result[i+1]
               }
               else
                    newRes.push(result[i])
          }
          return newRes;
     }
})();