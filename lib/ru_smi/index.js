module.exports = new (function () {
     var gend = "m";
     var group = {
          ed: 1,
          dec: 2,
          sot: 3
     };
     var phr = require("./ru");

     /**
      *
      * @param ind
      * @param start
      * @returns {boolean}
      */
     function isDigit(ind, start) {
          return !(ind < start || (ind - start) % 3 != 0);
     }

     /**
      *
      * @param key
      * @param ind
      * @param g
      * @returns {*}
      */
     function getText(key, ind, g) {
          ind = key == "unitsT" ? ind - 1 : ind;
          if (key == "units" && gend == "w" && [1, 2].indexOf(ind) >= 0 && g)
               return phr.text.unitsT[ind - 1];
          else
               return phr.text[key][ind];
     }

     /**
      *
      * @param ind
      * @param key
      * @returns {*}
      */
     function getExponent(ind, key) {
          ind = Math.round(ind > (phr.exponent.length - 1) * 3 + 1 ? 0 : ind / 3);
          var str = phr.ends[(ind == 1) ? 't' : 'm'][key];
          return phr.exponent[ind] + ((str != undefined) ? str : "");
     }

     /**
      *
      * @param g
      */
     this.setGender = function (g) {
          if (g != undefined)
               gend = g;
     };
     /**
      *
      * @param numb
      * @returns {*}
      */
     this.convert = function (numb) {
          var result = [];
          var arr = numb.split("").reverse();
          var fl = false;
          var len = arr.length;
          if (len == 1 && arr[0] == 0) {
               result.push(getText("units", 0, false));
               return this;
          }
          var raz = 3;
          while (!isDigit(len, raz--));
          raz++;
          do {
               if (arr[len - 1] != 0 && len != 0) {
                    var key, gend = false;
                    fl = true;
                    var index = arr[len - 1] - 1;
                    switch (raz) {
                         case group.ed: //единицы
                              key = (arr[len - 1] < 3 && len == 4) ? "unitsT" : "units";
                              gend = len == 1;
                              index++;
                              break;
                         case group.dec: //десятки
                              if (arr[len - 1] < 2 && arr[len - 2] != 0) {
                                   key = "to20";
                                   index = arr[len - 2] - 1;
                                   arr[len - 2] = 0;
                              } else
                                   key = "decade";
                              break;
                         case group.sot: // сотни
                              key = "hundreds";
                              break;
                    }
                    result.push(getText(key, index, gend));
               }
               if (fl && isDigit(len, 4)) {
                    fl = false;
                    var key;
                    var nch = (arr.length - len) >= 1 ? arr[len] * 10 + arr[len - 1] : arr[len - 1];
//какое окончание добавить
                    if (nch == 0 || nch >= 5 && nch < 20 || nch % 10 == 0 || nch % 10 >= 5 && nch % 10 < 10)
                         key = "d5_19";
                    else if (nch == 1 || nch % 10 == 1)
                         key = "d1";
                    else
                         key = "d2_4";
                    result.push(getExponent(len - 1, key));
               }
               raz == 1 ? raz = 3 : --raz;
          } while (--len);
          return result;
     };
})();