example: var numbertoword = require("numbertoword");

1. - numbertoword.init('ru' | 'ua' | 'en').setGender('m'|'w').convert('1234567890').getString();
2. - numbertoword.init('ru' | 'ua' | 'en').setGender('m'|'w').convert('1234567890').getArray();
3. - numbertoword.init().convert(1234567890).getArray();
4. - numbertoword.init().convert(1234567890).getString();

setGender() - not necessary. As default is 'm';

Language - 'ru' as default;